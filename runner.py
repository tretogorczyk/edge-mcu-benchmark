from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import os
import sys
import argparse

from logging import DEBUG, log, basicConfig, info

from tools import logger, run_experiment

parser = argparse.ArgumentParser(description='Run ML experiments.')
parser.add_argument('config_paths', metavar='PATH', type=str, nargs='+',
                    help='path to experiment directory with a config.json file')

# parser.add_argument('--sum', dest='accumulate', action='store_const', const=sum, default=max,
#                     help='sum the integers (default: find the max)')

info("starting runner")
args = parser.parse_args()
id = 0
for experiment_definition in args.config_paths:
    info(f"### commencing experiment {id}: {experiment_definition}")
    run_experiment.start(experiment_definition, dry_run=False)
    id += 1

