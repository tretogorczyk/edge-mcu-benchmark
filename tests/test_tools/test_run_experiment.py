import unittest
import json
import os
import sys

from tools import run_experiment


sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class TestRunExperiment(unittest.TestCase):

    def test_check_config_path_not_correct(self):
        with self.assertRaises(NameError):
            run_experiment.start(None)

        with self.assertRaises(NameError):
            run_experiment.start(None)

    def test_config_correct_experiment_starts(self):
        self.assertEqual(run_experiment.start("test_tools/sample", dry_run=True), 0)