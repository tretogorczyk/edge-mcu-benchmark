import unittest
import json
import os
import sys

import tools.parse_config as parse_config

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(os.path.dirname(os.path.realpath(__file__)))


class TestCheckpointSettings(unittest.TestCase):
    def setUp(self):
        sample_config_path = "test_tools/sample/test_config.json"
        with open(sample_config_path) as json_file:
            data = json.load(json_file)
            self.data = data["training"]["checkpoint"]
        parse_config.init_config(f'{sample_config_path}')

    def test_checkpoint_config_exists(self):
        self.assertIsNotNone(self.data)

    def test_checkpoint_path_exists(self):
        self.assertIsNotNone(self.data["path"])

    def test_check_get_checkpoint_path(self):
        self.assertIsNotNone(parse_config.get_checkpoint_path())
        self.assertEqual(self.data["path"], parse_config.get_checkpoint_path())

    def test_checkpoint_name_includes_corect_extension(self):
        checkpoint_name = parse_config.get_checkpoint_path()
        self.assertEqual(checkpoint_name.split(".")[-1], "ckpt")

    def test_checkpoint_raises_error_when_extension_incorrect(self):
        temp_name = "temp.json"
        with open(temp_name, 'w') as json_file:
            json_file.write('''{ 
    "name": "cifar10_dataset",
    "training": {
        "checkpoint": {
            "save_freq": 5,
            "path": "checkpoint"
        }
    }
}''')
        parse_config.init_config(f'{temp_name}')
        with self.assertRaises(parse_config.CheckpointFormatIncorrect):
            parse_config.get_checkpoint_path()
        os.remove(temp_name)

    def test_checkpoint_raises_error_when_save_freq_illegal_string(self):
        temp_name = "temp.json"
        with open(temp_name, 'w') as json_file:
                json_file.write('''{ 
        "name": "cifar10_dataset",
        "training": {
            "checkpoint": {
                "save_freq": "blah",
                "path": "checkpoint.ckpt"
            }
        }
    }''')
        parse_config.init_config(f'{temp_name}')
        with self.assertRaises(parse_config.CheckpointSaveFreqIllegal):
            parse_config.get_checkpoint_save_freq()
        os.remove(temp_name)

    def test_checkpoint_raises_error_when_save_freq_illegal_float(self):
        temp_name = "temp.json"
        with open(temp_name, 'w') as json_file:
                json_file.write('''{ 
        "name": "cifar10_dataset",
        "training": {
            "checkpoint": {
                "save_freq": "2.0",
                "path": "checkpoint.ckpt"
            }
        }
    }''')
        parse_config.init_config(f'{temp_name}')
        with self.assertRaises(parse_config.CheckpointSaveFreqIllegal):
            parse_config.get_checkpoint_save_freq()
        os.remove(temp_name)

    def test_checkpoint_correct_save_freq_value(self):
        self.assertEqual(self.data["save_freq"], parse_config.get_checkpoint_save_freq())


class TestInitConfig(unittest.TestCase):
    def test_incorrect_path(self):
        with self.assertRaises(TypeError):
            parse_config.init_config(None)
        with self.assertRaises(FileNotFoundError):
            parse_config.init_config("incorrect_path")

    def test_corrupted_config(self):
        with self.assertRaises(json.decoder.JSONDecodeError):
            parse_config.init_config("test_tools/sample/corrupted_test_config.json")


class TestBasicExperimentInformation(unittest.TestCase):
    def setUp(self):
        sample_config_path = "test_tools/sample/test_config.json"
        with open(sample_config_path) as json_file:
            self.data = json.load(json_file)
        parse_config.init_config(f'{sample_config_path}')

    def test_mandatory_fields_are_present(self):
        self.assertEqual(parse_config.get_experiment_name(), self.data["name"])
        self.assertEqual(parse_config.get_model_name(), self.data["model"])
        self.assertEqual(parse_config.get_dataset_name(), self.data["dataset"]["name"])
        self.assertEqual(parse_config.get_dataset_format(), self.data["dataset"]["format"])
        self.assertEqual(parse_config.get_optimizer_name(), self.data["training"]["optimizer"]["name"])

    def test_name_is_not_set_correctly(self):
        temp_name = "temp.json"
        with open(temp_name, 'w') as json_file:
            json_file.write('''{ 
            "training": {
                "checkpoint": {
                    "save_freq": "2.0",
                    "path": "checkpoint.ckpt"
                }
            }
        }''')
        parse_config.init_config(f'{temp_name}')
        with self.assertRaises(parse_config.IncorrectExperimentConfiguration):
            parse_config.get_experiment_name()
        os.remove(temp_name)


class TestCheckTrainingSettings(unittest.TestCase):
    def setUp(self):
        sample_config_path = "test_tools/sample/test_config.json"
        with open(sample_config_path) as json_file:
            self.data = json.load(json_file)["training"]
        parse_config.init_config(f'{sample_config_path}')

    def test_mandatory_fields_are_present(self):
        self.assertEqual(parse_config.get_epochs_count(), self.data["epochs"])

