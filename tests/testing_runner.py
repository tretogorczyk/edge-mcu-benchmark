import unittest
import sys
import os


# TEST_PATH = sys.argv[1]
# fname = "../tests/test_result.log"

#
# def print_suite(suite):
#     if hasattr(suite, '__iter__'):
#         for x in suite:
#             print_suite(x)
#     else:
#         print(suite)


# print(f"looking for tests in {TEST_PATH}")
# with open(fname, "w") as f:


loader = unittest.TestLoader()
suite = loader.discover(start_dir="/home/lukegrzym/code/ai/edge-mcu-benchmark/", pattern="*test_*")
runner = unittest.TextTestRunner(descriptions=True, verbosity=1)
runner.run(suite)


