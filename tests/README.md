
# 1. Instructions
####1.  Structure

Please make sure that the test directory matches that of the regular files. All test directories must include __init__.py file to be properly recognized by unittest framework.

####2. To execute the gitlab-runner and run all tests type:


gitlab-runner exec JOB_NAME

gitlab-runner exec test_build

# 2. Known-issues
####1. CI job is stuck

If CI pipeline job is "pending", runner is stuck, you may check CI settings in gitlab. Likely, a runner is not connected. In this case you mean need to reconnect the runner with gitlab:

gitlab-runner verify
 


####2. Tests are not found and executed

CI requires each directory to be a package - add __init__ to each subdir. In python 3.X all dirs are packages, but for some reason this is not enough.


# 3. TODO:
1. Prepare a full job with installing venv, requirements in a docker image and running all tests
2. Prepare a job to train models per experiment configuration


