import unittest
import json
import os
import sys

from evaluate import saver


sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class Test_save_results(unittest.TestCase):

    def test_save_path_not_correct(self):
        with self.assertRaises(NameError):
            saver.save_results(None, "")

        with self.assertRaises(TypeError):
            saver.save_results(5, "")

    def test_correct_path_saves_results(self):
        pass

        # with self.assertRaises(NameError):
        #     run_experiment.start(None)
    #
    # def test_config_correct_experiment_starts(self):
    #     self.assertEqual(run_experiment.start("sample/test_config.json", dry_run=True), 0)