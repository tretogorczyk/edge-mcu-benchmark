import unittest
import json
import os
import sys

import tools.parse_config as parse_config

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from optimizer import optimizer


class TestOptimizer(unittest.TestCase):
    def setUp(self):
        pass

    def test_name_not_correct_raises_error(self):
        optimizer_name = "unknown"
        with self.assertRaises(KeyError):
            opt_instance = optimizer.create_optimizer(optimizer_name)

    def test_name_correct_instantiates_model(self):
        optimizer_name = "adam"
        opt_instance = optimizer.create_optimizer(optimizer_name)
        self.assertIsNotNone(opt_instance)