import unittest
import json
import os
import sys

import tools.parse_config as parse_config

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from models import model_creator


class TestModelName(unittest.TestCase):
    def setUp(self):
        pass

    def test_name_not_correct_raises_error(self):
        model_name = "unknown"
        with self.assertRaises(KeyError):
            model = model_creator.create_model(model_name)

    def test_name_correct_instantiates_model(self):
        model_name = "Cif10_Dense2_Simple"
        model = model_creator.create_model(model_name)
        self.assertIsNotNone(model)