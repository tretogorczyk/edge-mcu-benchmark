import unittest
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from dataset import dataset


class Test_get_dataset(unittest.TestCase):
    def setUp(self):
        pass

    def test_raises_NameError_when_input_args_none(self):
        with self.assertRaises(NameError):
            dataset.get_dataset("mnist", None)

        with self.assertRaises(NameError):
            dataset.get_dataset("num_arrays", None)

    def test_raises_TypeError_when_unsupported_dataset_name(self):
        with self.assertRaises(TypeError):
            dataset.get_dataset("mnistt", "num_arrays")

        with self.assertRaises(TypeError):
            dataset.get_dataset("num_arrayss", "num_arrays")

    def test_raises_TypeError_when_unsupported_format(self):
        with self.assertRaises(TypeError):
            dataset.get_dataset("mnist", "unknown")

        with self.assertRaises(TypeError):
            dataset.get_dataset("mnist", "xy")

    def test_returns_num_arrays_correctly(self):
        x, y, x1, y1 = dataset.get_dataset("mnist", "num_arrays")
        self.assertIsNotNone(x)
        self.assertIsNotNone(y)
        self.assertIsNotNone(x1)
        self.assertIsNotNone(y1)

        x, y, x1, y1 = dataset.get_dataset("cifar10", "num_arrays")
        self.assertIsNotNone(x)
        self.assertIsNotNone(y)
        self.assertIsNotNone(x1)
        self.assertIsNotNone(y1)

