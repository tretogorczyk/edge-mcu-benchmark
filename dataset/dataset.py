from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import os
import sys

from logging import info, debug

# For directly run scripts, add project root
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


def load_mnist_slices():
    (mnist_images, mnist_labels), _ = tf.keras.datasets.mnist.load_data()
    dataset = tf.data.Dataset.from_tensor_slices(
        (tf.cast(mnist_images[..., tf.newaxis] / 255, tf.float32),
         tf.cast(mnist_labels, tf.int64)))
    dataset = dataset.shuffle(1000).batch(32)
    return dataset

def load_mnist():
    mnist = tf.keras.datasets.mnist
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0
    return x_train, y_train, x_test, y_test


def load_cifar10():
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    return x_train, y_train, x_test, y_test


def get_dataset(dataset, format):
    """
    Checks, normalizes
    :param dataset: name of the dataset
    :param format: format in which to return dataset
    :return: numpy arrays  x_train, y_train, x_test, y_test; tensors; etc
    """
    info(f"dataset: {dataset}")
    if dataset is None or format is None:
        raise NameError

    if format == "num_arrays":
        if dataset == "mnist":
            return load_mnist()
        if dataset == "cifar10":
            return load_cifar10()
    elif format == "tensor_slices":
        if dataset == "mnist":
            return load_mnist_slices()
    info("dataset not found")
    raise TypeError
