from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import os
import sys

from logging import info, debug

# For directly run scripts, add project root
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


def create_optimizer(optimizer):
    info(f"optimizer: {optimizer}")
    if optimizer == "adam":
        return tf.keras.optimizers.Adam()
    #opt = keras.optimizers.RMSprop(learning_rate=0.0001, decay=1e-6)
    else:
        info("optimizer not found")
        raise KeyError
