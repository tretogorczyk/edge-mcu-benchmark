from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf

from tensorflow.keras.models import Sequential


class Cif10_Dense2_Simple(Sequential):
    def __init__(self):
        Sequential.__init__(self)
        self.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
        self.add(tf.keras.layers.Dense(128, activation='relu'))
        self.add(tf.keras.layers.Dropout(0.2))
        self.add(tf.keras.layers.Dense(10, activation='softmax'))

def get_model():
    return Cif10_Dense2_Simple()
