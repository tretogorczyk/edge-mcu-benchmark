from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import os
import sys

from logging import info, debug

# For directly run scripts, add project root
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


def create_model(model_name):
    info(f"model name: {model_name}")
    if model_name == "Cif10_Dense2_Simple":
        from models import cif10_dense2_simple
        return cif10_dense2_simple.get_model()
    else:
        info("model not found")
        raise KeyError
