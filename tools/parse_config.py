import json
import os
import sys
from logging import DEBUG, log, basicConfig, info, debug

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

config_data = None
config_checkpoint = None


class CheckpointFormatIncorrect(Exception):
    """
    Raised when checkpoint path does not include .ckpt extension
    """
    pass


class CheckpointSaveFreqIllegal(Exception):
    """
    Raised when save frequency illegal value
    """
    pass


class IncorrectExperimentConfiguration(Exception):
    """
    Raised when incorrect configuration is found
    """
    pass


def init_config(config_path):
    """
    Loads and initializes experiment configuration script
    :param config_path: path to the configuration file
    :return: None
    """
    with open(config_path) as json_file:
        global config_data, config_checkpoint
        config_data = json.load(json_file)
        try:
            config_checkpoint = config_data["training"]["checkpoint"]
        except KeyError:
            debug("checkpoints not configured")


def get_checkpoint_config():
    pass


def get_checkpoint_path():
    ckpt_path = config_checkpoint["path"]
    if ckpt_path.split(".")[-1] != "ckpt":
        raise CheckpointFormatIncorrect
    return ckpt_path


def get_checkpoint_save_freq():
    ckpt_freq = config_checkpoint["save_freq"]
    if ckpt_freq != "epoch" and type(ckpt_freq) != int:
        raise CheckpointSaveFreqIllegal
    return config_checkpoint["save_freq"]


def get_experiment_name():
    try:
        return config_data["name"]
    except KeyError:
        raise IncorrectExperimentConfiguration


def get_epochs_count():
    return config_data["training"]["epochs"]


def get_model_name():
    return config_data["model"]


def get_dataset_name():
    return config_data["dataset"]["name"]


def get_dataset_format():
    return config_data["dataset"]["format"]


def get_optimizer_name():
    return config_data["training"]["optimizer"]["name"]