from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import os
import sys

from logging import DEBUG, log, basicConfig, info

from tools import parse_config, logger
from models import model_creator
from dataset import dataset
from optimizer import optimizer

# For directly run scripts, add project root
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


def get_checkpoint_callback(config_path):
    checkpoint_path = f'{config_path}/{parse_config.get_checkpoint_path()}'
    checkpoint_freq = parse_config.get_checkpoint_save_freq()
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                     save_weights_only=True,
                                                     save_freq=checkpoint_freq,
                                                     verbose=1)
    return cp_callback


def start(config_path, dry_run=False):
    if config_path is None: # or config_path.split(".")[-1] != "json":
        info("incorrect experiment config path")
        raise NameError

    info("parsing configuration")

    parse_config.init_config(f'{config_path}/config.json')
    name = parse_config.get_experiment_name()
    epochs = parse_config.get_epochs_count()
    model_name = parse_config.get_model_name()

    info("spawning model")
    model = model_creator.create_model(model_name)

    info("preparing checkpoint callback")
    cp_callback = get_checkpoint_callback(config_path)

    info("preparing dataset")
    x_train, y_train, x_test, y_test = dataset.get_dataset(parse_config.get_dataset_name(), parse_config.get_dataset_format())

    info("preparing optimizer")
    opt_instance = optimizer.create_optimizer(parse_config.get_optimizer_name())

    model.compile(optimizer=opt_instance,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'], )

    if dry_run:
        info("dry run enabled, skipping training and returning")
        return 0

    info("training")
    model.fit(x_train, y_train, epochs=epochs, callbacks=[cp_callback])
    model.evaluate(x_test, y_test, verbose=2)
    return model

# config_path = "test_1_config.json"
# start(config_path)





