from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import os
import sys

from logging import DEBUG, log, basicConfig, info

from tools import parse_config, logger
from models import model_creator
from dataset import dataset
from optimizer import optimizer

# For directly run scripts, add project root
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


def get_checkpoint_callback():
    checkpoint_path = parse_config.get_checkpoint_path()
    checkpoint_freq = parse_config.get_checkpoint_save_freq()
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                     save_weights_only=True,
                                                     save_freq=checkpoint_freq,
                                                     verbose=1)
    return cp_callback


def start(config_path):
    info("starting test")

    parse_config.init_config(config_path)
    name = parse_config.get_experiment_name()
    epochs = parse_config.get_epochs_count()
    model_name = parse_config.get_model_name()

    model = model_creator.create_model(model_name)
    cp_callback = get_checkpoint_callback()
    x_train, y_train, x_test, y_test = dataset.get_dataset(parse_config.get_dataset_name(), parse_config.get_dataset_format())
    optimizer_name = parse_config.get_optimizer_name()
    opt_instance = optimizer.create_optimizer(optimizer_name)

    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir="log_dir", histogram_freq=1)

    model.compile(optimizer=opt_instance,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'],
                  validation_data=(x_test, y_test))

    model.fit(x_train, y_train, epochs=epochs, callbacks=[cp_callback, tensorboard_callback])
    result = model.evaluate(x_test, y_test, verbose=2)
    print(result)
    return model


config_path = "test_1_config.json"
start(config_path)





